//
//  TopTableViewCell.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import UIKit


class TopTableViewCell: UITableViewCell {
    
    var didTapAction: ((Int) -> ())?
    
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(item: Top, isFavorite: Bool){
        if let url = URL(string: item.imageURL){
            mImageView.load(with: url, placeholder: nil)
        }
        titleLabel.text = item.title
        rankLabel.text = "\(item.rank)"
        typeLabel.text = item.type.toString()
        startDateLabel.text = item.startDate
        endDateLabel.text = item.endDate
        actionButton.setTitle( isFavorite ?  "Remove" : "Add" , for: .normal)
    }
    @IBAction func didClickAction(){
        didTapAction?(actionButton.tag)
    }
}

