//
//  APIManager.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

public enum APIManagerSurrounding {
    case production
    case mock(MockURLSession)
}

public class APIManager: APIManagerProtocol {
    
    public static var shared: APIManager = APIManager()
    
    private lazy var networkController: NetworkControllerProtocol = {
        switch environment{
        case .production:
            return NetworkController()
        case .mock(let mockSession):
            return NetworkController(session: mockSession)
        }
    }()
    
    public lazy var topAPI: TopAPIProtocol = {
        return TopAPIController(networkController: self.networkController)
    }()
    
    fileprivate var environment: APIManagerSurrounding = .production
    
    init(surrounding: APIManagerSurrounding = .production) {
        self.environment = surrounding
    }
    
    public static func toggle(surrounding: APIManagerSurrounding){
        APIManager.shared = APIManager(surrounding: surrounding)
    }
}
