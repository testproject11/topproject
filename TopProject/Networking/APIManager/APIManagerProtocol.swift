//
//  APIManagerProtocol.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

public protocol APIManagerProtocol {
    var topAPI: TopAPIProtocol { get set }
}
