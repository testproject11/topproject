//
//  NetworkAPIController.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/8.
//

import Foundation

public typealias ResponseCompletion<T> = (Result<T, Error>) -> ()


extension APIControllerProtocol {
    func request<T>(specificEndpoit: EndPointType, completionQueue: DispatchQueue?, completion: @escaping ResponseCompletion<T>) where T: ResponseType{
        
        func performCompletion(result: Result<T, Error>) {
            if let completionQueue = completionQueue {
                completionQueue.async {
                    completion(result)
                }
            } else {
                completion(result)
            }
        }
        networkController.request(endpoint: specificEndpoit) { (result) in
            let modularResult = result.flatMap { (data) -> Result<T, Error> in
                return Result { try JSONDecoder().decode(T.self, from: data) }
            }
            performCompletion(result: modularResult)
        }
    }
}
