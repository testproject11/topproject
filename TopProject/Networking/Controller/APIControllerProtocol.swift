//
//  File.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/8.
//

import Foundation


protocol APIControllerProtocol {
    var networkController: NetworkControllerProtocol { get }
    func request<T>(specificEndpoit: EndPointType, completionQueue: DispatchQueue?, completion: @escaping ResponseCompletion<T>) where T: ResponseType
}
