//
//  BaseAPIController.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/12.
//

import Foundation

public class BaseAPIController<EndPoint>: APIControllerProtocol where EndPoint: EndPointType {
    var networkController: NetworkControllerProtocol
    init(networkController: NetworkControllerProtocol) {
        self.networkController = networkController
    }
    
}
