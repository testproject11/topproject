//
//  NetworkController+.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

public class NetworkController: NetworkControllerProtocol {
    func request(endpoint: EndPointType, handler: @escaping (Result<Data, Error>) -> ()) {
        
        let request = URLRequestFactory.createRequest(forEndpoint: endpoint)
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                handler(.failure(APIError.network(error)))
                return
            }
            handler(.success(data!))
        }
        task.resume()
    }
    
    private var urlSession: URLSessionProtocol = URLSession.shared
    
    public init() {
    }
    
    convenience init(session: URLSessionProtocol) {
        self.init()
        self.urlSession = session
    }
}
