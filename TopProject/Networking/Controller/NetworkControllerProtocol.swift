//
//  NetworkControllerProtocol.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

protocol NetworkControllerProtocol {
    func request(endpoint: EndPointType,
                 handler: @escaping (Result<Data, Error>) -> ())
}
