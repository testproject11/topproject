//
//  TopAPIController.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/8.
//

import Foundation

public class TopAPIController: BaseAPIController<TopEndpoint> {
    
}

extension TopAPIController: TopAPIProtocol{
    public func getTop(type: TopType,
                       page: Int,
                       subtype: SubTypeProtocol,
                       completion: @escaping ResponseCompletion<GetTopResponse> ){
        request(specificEndpoit: TopEndpoint.top(type: type, page: page, subtype: subtype), completionQueue: .main, completion: completion)
    }
}
