//
//  TopAPIProtocol.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/8.
//

import Foundation


public protocol TopAPIProtocol{
    func getTop(type: TopType,
                page: Int,
                subtype: SubTypeProtocol,
                completion: @escaping ResponseCompletion<GetTopResponse> )
}

