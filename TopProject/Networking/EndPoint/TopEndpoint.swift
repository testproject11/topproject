//
//  TopEndpoint.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

public enum TopType: Int, CaseIterable {
    case anime = 0
    case manga
}

extension TopType{
    func toString() -> String{
        switch self {
        case .anime:
            return "Anime"
        case .manga:
            return "Manga"
        }
    }
    func stringToEnum(str: String) -> TopType{
        switch str {
        case "anime":
            return .anime
        case "manga":
            return .manga
        default:
            return .anime
        }
    }
}

public enum TopEndpoint: EndPointType {
    case top(type: TopType, page: Int?, subtype: SubTypeProtocol?)
}

extension TopEndpoint {
    public var url: URL {
        switch self {
        case .top(let type,let page,let subtype):
            return URL.createURL().appendQueryItem(value: type.toString().lowercased()).appendQueryItem(value: page).appendQueryItem(value: subtype?.toString().lowercased())
        }
    }
}
