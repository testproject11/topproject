//
//  APIError.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation
public enum APIError: Error {
    case network(_ detail: Error)
}
