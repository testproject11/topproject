//
//  MockURLSession.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import Foundation
public class MockURLSession: URLSessionProtocol {
    private (set) var lastURL: URL?
    var nextDataTask = MockURLSessionDataTask()
    var nextData: Data?
    var nextError: Error?
    func successHttpURLResponse(request: URLRequest) -> URLResponse {
        return HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    }
    
    public func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTaskProtocol {
        
        lastURL = request.url
        
        completionHandler(nextData, successHttpURLResponse(request: request), nextError)
        
        return nextDataTask
    }
    
}
