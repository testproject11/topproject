//
//  URLSessionDataTaskProtocol.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import Foundation
public protocol URLSessionDataTaskProtocol {
    func resume()
}
