//
//  URLSessionProtocol.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import Foundation
public protocol URLSessionProtocol {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

    func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTaskProtocol
}
