//
//  URLRequestFactory.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation
struct URLRequestFactory {
    
    static func createRequest(forEndpoint endpoint: EndPointType,
                              httpMethod: HTTPMethod = .get) -> URLRequest {
        print(endpoint.url.absoluteURL)
        var request: URLRequest = URLRequest(url: endpoint.url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        request.httpMethod = httpMethod.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allowsCellularAccess = true
        return request
    }
    
    
}
