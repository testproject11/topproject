//
//  Top+Decode.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/10.
//

import Foundation


extension Top: Decodable {
    enum CodingKeys: String, CodingKey {
        case malID = "mal_id"
        case rank, title, url
        case imageURL = "image_url"
        case type, episodes
        case startDate = "start_date"
        case endDate = "end_date"
        case members, score
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        malID = try container.decode(Int.self, forKey: .malID)
        rank = try container.decode(Int.self, forKey: .rank)
        title = try container.decode(String.self, forKey: .title)
        url = try container.decode(String.self, forKey: .url)
        imageURL = try container.decode(String.self, forKey: .imageURL)
        let subType = try container.decode(String.self, forKey: .type)
        print(subType.lowercased())
        type = Top.subTypeToEnum(str: subType.lowercased())
        episodes = try container.decodeIfPresent(Int.self, forKey: .episodes)
        startDate = try container.decodeIfPresent(String.self, forKey: .startDate)
        endDate = try container.decodeIfPresent(String.self, forKey: .endDate)
        members = try container.decode(Int.self, forKey: .members)
        score = try container.decode(Double.self, forKey: .score)
    }
    
    static func subTypeToEnum(str: String) -> SubTypeProtocol{
        switch str {
        case "movie":
            return SubType.Anime.movie
        case "ona":
            return SubType.Anime.ona
        case "ova":
            return SubType.Anime.ova
        case "tv":
            return SubType.Anime.tv
        case "upcoming":
            return SubType.Anime.upcoming
        case "unknown":
            return SubType.Anime.unknown
        case "airing":
            return SubType.Anime.airing
        case "special":
            return SubType.Anime.special
        case "bypopularity":
            return SubType.bypopularity
        case "favorite":
            return SubType.favorite
        case "manga":
            return SubType.Manga.manga
        case "novel", "novels":
            return SubType.Manga.novels
        case "oneshots":
            return SubType.Manga.oneshots
        case "doujin":
            return SubType.Manga.doujin
        case "manhwa":
            return SubType.Manga.manhwa
        case "manhua":
            return SubType.Manga.manhua
        case "one":
            return SubType.Manga.one
        case "doujinshi":
            return SubType.Manga.doujinshi
        default:
            return SubType.Anime.movie
        }
    }
}
