//
//  Top+Encode.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

extension Top: Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(malID, forKey: .malID)
        try container.encode(rank, forKey: .rank)
        try container.encode(title, forKey: .title)
        try container.encode(rank, forKey: .rank)
        try container.encode(url, forKey: .url)
        try container.encode(imageURL, forKey: .imageURL)
        try container.encode(type.toString() , forKey: .type)
        try container.encode(episodes, forKey: .episodes)
        try container.encode(startDate, forKey: .startDate)
        try container.encode(endDate, forKey: .endDate)
        try container.encode(members, forKey: .members)
        try container.encode(score, forKey: .score)
    }
}
