//
//  Top.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/10.
//

import Foundation

//typealias SubTypeMain = SubType & SubType.Anime & SubType.Manga

public protocol SubTypeProtocol {
    func toString() -> String
}

public enum SubType: SubTypeProtocol {
    case bypopularity
    case favorite
    public enum Anime: Int, CaseIterable, SubTypeProtocol {
        case movie
        case ona
        case ova
        case tv
        case upcoming
        case unknown
        case airing
        case special
        case bypopularity
        case favorite
    }
    public enum Manga: Int, CaseIterable, SubTypeProtocol {
        case manga
        case novels
        case oneshots
        case doujin
        case manhwa
        case manhua
        case bypopularity
        case favorite
        case one
        case doujinshi
    }
}

extension SubType{
    public func toString() -> String{
        switch self {
        case .bypopularity:
            return "Bypopularity"
        case .favorite:
            return "Favorite"
        }
    }
}

extension SubType.Anime{
    public  func toString() -> String{
        switch self {
        case .movie:
            return "Movie"
        case .ona:
            return "Ona"
        case .ova:
            return "Ova"
        case .tv:
            return "TV"
        case .unknown:
            return "Unknown"
        case .upcoming:
            return "Upcoming"
        case .airing:
            return "Airing"
        case .special:
            return "Special"
        case .bypopularity:
            return "Bypopularity"
        case .favorite:
            return "Favorite"
        }
    }
}

extension SubType.Manga{
    public func toString() -> String{
        switch self {
        case .manga:
            return "Manga"
        case .novels:
            return "Novels"
        case .oneshots:
            return "Oneshots"
        case .doujin:
            return "Doujin"
        case .manhwa:
            return "Manhwa"
        case .manhua:
            return "Manhua"
        case .bypopularity:
            return "Bypopularity"
        case .favorite:
            return "Favorite"
        case .one:
            return "One"
        case .doujinshi:
            return "Doujinshi"
        }
    }
}

public struct Top {
    let malID, rank: Int
    let title: String
    let url: String
    let imageURL: String
    let type: SubTypeProtocol
    let episodes: Int?
    let startDate, endDate: String?
    let members: Int
    let score: Double
}
