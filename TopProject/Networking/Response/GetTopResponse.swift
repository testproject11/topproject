//
//  File.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/8.
//

import Foundation

public struct GetTopResponse: Codable, ResponseType{
    let requestHash: String?
    let requestCached: Bool?
    let requestCacheExpiry: Int?
    let top: [Top]?
    
    enum CodingKeys: String, CodingKey {
        case requestHash = "request_hash"
        case requestCached = "request_cached"
        case requestCacheExpiry = "request_cache_expiry"
        case top
    }
}
