//
//  HTTPMethod.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation
public enum HTTPMethod: String{
    case get = "GET"
}
