//
//  URL+.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/11.
//

import Foundation

extension URL {
    static func createURL() -> URL {
        return URL(string: String(format: "%@", Configurations.baseUrl))!
    }
    
    func appendQueryItem(value: String?) -> URL {
        guard value != nil else {
            return self
        }
        return self.appendingPathComponent(value!)
    }
    func appendQueryItem(value: Int?) -> URL {
        guard value != nil else {
            return self
        }
        return self.appendingPathComponent("\(value!)")
    }
}
