//
//  DataPersistence.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import Foundation


class DataPersistence {
    
    var filePath : URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("data")
    }
    
    func storeTop(tops: [Top]) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(tops)
            let documentDirectory = try FileManager.default.url(for: .documentDirectory,
                                                                in: .userDomainMask,
                                                                appropriateFor:nil,
                                                                create:false)
            let url = documentDirectory.appendingPathComponent(String(describing: Top.self))
            try data.write(to: url)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // Retrieve an Array of products
    func retrieveTop() -> [Top]? {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory,
                                                                in: .userDomainMask,
                                                                appropriateFor:nil,
                                                                create:false)
            let url = documentDirectory.appendingPathComponent(String(describing: Top.self))
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let tops = try decoder.decode([Top].self, from: data)
            return tops
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
