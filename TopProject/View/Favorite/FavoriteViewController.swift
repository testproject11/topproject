//
//  FavoriteViewController.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/14.
//

import UIKit
class FavoriteViewController: UIViewController {
    let viewModel = FavoriteViewModel()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupViewModel()
    }
    func setupUI(){
        tableView.register(UINib(nibName: String(describing: TopTableViewCell.self),
                                 bundle: nil),
                           forCellReuseIdentifier: String(describing: TopTableViewCell.self))
        tableView.rowHeight = 100
    }
    
    func setupViewModel(){
        viewModel.didUpdatedData = { [weak self] in
            guard let strongSelf = self else{
                return
            }
            strongSelf.tableView.reloadData()
        }
    }
    
    deinit {
        print("deinit")
    }
}

extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopTableViewCell.self)) as? TopTableViewCell else {
            return UITableViewCell()
        }
        let top = viewModel.itemAtIndex(indexPath.row)
        cell.config(item: top, isFavorite: true)
        cell.selectionStyle = .none
        cell.actionButton.tag = indexPath.row
        cell.didTapAction = didTapAction()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        let item = viewModel.itemAtIndex(indexPath.row)
        performSegue(withIdentifier: "webIdentifier", sender: item.url)
    }
}

extension FavoriteViewController{
    private func didTapAction() -> ((_ index: Int) -> ()) {
        return { [weak self] index in
            guard let strongSelf = self else {
                return
            }
            strongSelf.viewModel.removeFavorite(with: index)
        }
    }
}
