//
//  FavoriteViewModel.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/14.
//

import Foundation

class FavoriteViewModel {
    typealias CompletionHandler = () -> Void
    
    var didUpdatedData: CompletionHandler?
   
    private var favoriteData: [Top] = [] {
        didSet {
            didUpdatedData?()
        }
    }
    
    init() {
        getFavorites()
    }
    
    func getFavorites(){
        let persistence = DataPersistence()
        favoriteData = persistence.retrieveTop() ?? []
    }
    
    func removeFavorite(with index: Int) {
        let persistence = DataPersistence()
        favoriteData.remove(at: index)
        persistence.storeTop(tops: favoriteData)
    }
}


extension FavoriteViewModel{
    func numberOfSections() -> Int {
        return favoriteData.count
    }
    func itemAtIndex(_ index: Int) -> Top {
        return favoriteData[index]
    }
}
