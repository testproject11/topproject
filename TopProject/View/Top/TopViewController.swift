//
//  ViewController.swift
//  TopProject
//
//  Created by Polo Jhuo on 2021/2/4.
//

import UIKit

class TopViewController: UIViewController {
    typealias SubTypeMain = SubTypeProtocol

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.register(UINib(nibName: String(describing: TopTableViewCell.self),
                                     bundle: nil),
                               forCellReuseIdentifier: String(describing: TopTableViewCell.self))
            tableView.rowHeight = 100
        }
    }
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var subtypeTextField: UITextField!
    
    private let viewModel = TopViewModel()
    private var typePicker = UIPickerView()
    private var subtypePicker = UIPickerView()
    private var selectedType: TopType?
    private var selectedSubtype: SubTypeMain?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupUI()
        setupViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.setFavorites()
    }
    
    func setupUI(){
        typePicker.tag = 1
        subtypePicker.tag = 2
        typePicker.delegate = self
        typePicker.dataSource = self
        subtypePicker.delegate = self
        subtypePicker.dataSource = self
        typeTextField.inputView = typePicker
        subtypeTextField.inputView = subtypePicker
    }
    
    func setupViewModel(){
        viewModel.didUpdatedData = { [weak self] in
            guard let strongSelf = self else{
                return
            }
            strongSelf.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webViewController = segue.destination as? WebViewController,
           let url = sender as? String{
            webViewController.url = URL(string: url)
        }
    }
    
}

extension TopViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopTableViewCell.self)) as? TopTableViewCell else {
            return UITableViewCell()
        }
        let top = viewModel.itemAtIndex(indexPath.row)
        cell.config(item: top,
                    isFavorite: viewModel.isFavoriteExict(with: top.malID))
        cell.selectionStyle = .none
        cell.actionButton.tag = indexPath.row
        cell.didTapAction = didTapAction()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        let item = viewModel.itemAtIndex(indexPath.row)
        performSegue(withIdentifier: "webIdentifier", sender: item.url)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard selectedType != nil , selectedSubtype != nil else {
            return
        }
        
        if viewModel.numberOfSections() - 10 < indexPath.row{
            viewModel.loadMore(type: selectedType!,
                               subtype: selectedSubtype!)
        }
    }
}

extension TopViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            //type
            return TopType.allCases.count
        }else{
            //subtype
            if selectedType == .anime{
                return SubType.Anime.allCases.count
            }else{
                return SubType.Manga.allCases.count
            }
           
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            //type
            selectedType = TopType(rawValue: row)
            typeTextField.text = selectedType?.toString()
            selectedSubtype = nil
            subtypeTextField.text = ""
        }else{
            if selectedType == .anime{
                selectedSubtype = SubType.Anime(rawValue: row)
                subtypeTextField.text = selectedSubtype?.toString()
            }else{
                selectedSubtype = SubType.Manga(rawValue: row)
                subtypeTextField.text = selectedSubtype?.toString()
            }
        }
        if let type = selectedType,
           let subtype = selectedSubtype{
            viewModel.fetchData(type: type, subtype: subtype)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            //type
            return TopType(rawValue: row)?.toString()
        }else{
            if selectedType == .anime{
                return SubType.Anime(rawValue: row)?.toString()
            }else{
                return SubType.Manga(rawValue: row)?.toString()
            }
        }
    }
    
}


extension TopViewController{
    private func didTapAction() -> ((_ index: Int) -> ()) {
        return { [weak self] index in
            guard let strongSelf = self else {
                return
            }
            let top = strongSelf.viewModel.itemAtIndex(index)
            if strongSelf.viewModel.isFavoriteExict(with: top.malID){
                strongSelf.viewModel.removeFavorite(with: top)
            }else{
                strongSelf.viewModel.saveFavorite(with: top)
            }
        }
    }
}
