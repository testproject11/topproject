//
//  TopViewModel.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import Foundation

class TopViewModel {
    typealias SubTypeMain = SubTypeProtocol
    typealias CompletionHandler = () -> Void
    
    var didUpdatedData: CompletionHandler?
    
    private var isLoading = false
    private var page = 1
    private var likeData: [Int:Top] = [:] {
        didSet {
            didUpdatedData?()
        }
    }
    
    private var topData: [Top] = [] {
        didSet {
            didUpdatedData?()
        }
    }
    
    init() {
        setFavorites()
    }
    
    func fetchData(type: TopType, subtype: SubTypeMain){
        guard !isLoading else {
            return
        }
        isLoading = true
        page = 1
        APIManager.shared.topAPI.getTop(type: type, page: page, subtype: subtype) { [weak self] (result) in
            guard let strongSelf = self else{
                return
            }
            
            switch result{
            case .success(let response):
                strongSelf.topData = response.top ?? []
            case .failure(let error):
                print(error)
            }
            strongSelf.isLoading = false
        }
    }
    func loadMore(type: TopType, subtype: SubTypeMain) {
        guard !isLoading else {
            return
        }
        isLoading = true
        page = page + 1
        APIManager.shared.topAPI.getTop(type: type, page: page, subtype: subtype) { [weak self] (result) in
            guard let strongSelf = self else{
                return
            }
            switch result{
            case .success(let response):
                if let top = response.top , top.count > 0 {
                    strongSelf.topData.append(contentsOf: top)
                }else{
                    strongSelf.page = strongSelf.page - 1
                }
            case .failure(let error):
                print(error)
                strongSelf.page = strongSelf.page - 1
            }
            strongSelf.isLoading = false
        }
    }
    
    func saveFavorite(with top: Top){
        let persistence = DataPersistence()
        likeData[top.malID] = top
        persistence.storeTop(tops: Array(likeData.values))
    }
    
    func removeFavorite(with top: Top) {
        let persistence = DataPersistence()
        likeData[top.malID] = nil
        persistence.storeTop(tops: Array(likeData.values))
    }
    
    func setFavorites(){
        let persistence = DataPersistence()
        let tops = persistence.retrieveTop() ?? []
        likeData = tops.reduce(into: [Int: Top]()) {
            $0[$1.malID] = $1
        }
    }
    
    func isFavoriteExict(with id: Int) -> Bool{
        return likeData[id] != nil
    }
}

extension TopViewModel{
    func numberOfSections() -> Int {
        return topData.count
    }
    func itemAtIndex(_ index: Int) -> Top {
        return topData[index]
    }
}
