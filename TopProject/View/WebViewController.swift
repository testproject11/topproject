//
//  WebViewController.swift
//  TopProject
//
//  Created by Jhuo Yu cheng on 2021/2/13.
//

import UIKit
import WebKit
class WebViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    public var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUrl()
    }
    
    fileprivate func loadUrl() {
        if let url = url {
            webView.load(URLRequest(url: url))
        }
    }
    
    deinit {
        print("deinit")
    }
}
  
