//
//  TopProjectTests.swift
//  TopProjectTests
//
//  Created by Polo Jhuo on 2021/2/4.
//

import XCTest
@testable import TopProject

class TopProjectTests: XCTestCase {
    
    let urlSession = MockURLSession()
    var data: Data? = nil
    
    let viewModel = TopViewModel()
    
    override func setUp() {
        super.setUp()
        APIManager.toggle(surrounding: .mock(urlSession))
        
        let path = Bundle.main.path(forResource: "api", ofType: "txt")!
        data = FileManager.default.contents(atPath: path)
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_api_load_mock_data() {
        let exception = XCTestExpectation()
        urlSession.nextData = data
        
        var tops: [Top] = []
        APIManager.shared.topAPI.getTop(type: .anime,
                                        page: 1,
                                        subtype: SubType.Anime.movie) { (result) in
            switch result{
            case .success(let response):
                tops = response.top ?? []
            case .failure(let error):
                print(error)
            }
            exception.fulfill()
        }
        wait(for: [exception], timeout: 10)
        XCTAssertTrue(tops.count == 2)
    }
}

// test dataPersistence
extension TopProjectTests{
    func test_favorite_save_remove(){
        guard  let data = data else {
            XCTFail("the data is empty")
            return
        }
        let persistence = DataPersistence()
        persistence.storeTop(tops: [])
        let response = try! JSONDecoder().decode(GetTopResponse.self, from: data)
        if response.top?.count == 0{
            XCTFail("no top data")
        }
        //save 1 data
        persistence.storeTop(tops: [response.top!.first!])
        XCTAssertTrue(persistence.retrieveTop()?.count == 1)
    }
}

// test viewModel
extension TopProjectTests{
    // test numberOfSections
    func test_viewmodel_numberOfSections(){
        let exception = XCTestExpectation()
        urlSession.nextData = data
        viewModel.didUpdatedData = {
            exception.fulfill()
        }
        viewModel.fetchData(type: .anime, subtype: SubType.Anime.movie)
        wait(for: [exception], timeout: 10)
        XCTAssertTrue(viewModel.numberOfSections() == 2)
    }
    
    // test itemAtIndex
    func test_viewmodel_itemAtIndex(){
        guard  let data = data else {
            XCTFail("the data is empty")
            return
        }
        let exception = XCTestExpectation()
        let response = try! JSONDecoder().decode(GetTopResponse.self, from: data)
        urlSession.nextData = data
        viewModel.didUpdatedData = {
            exception.fulfill()
        }
        viewModel.fetchData(type: .anime, subtype: SubType.Anime.movie)
        wait(for: [exception], timeout: 10)
        XCTAssertTrue(viewModel.itemAtIndex(0).malID == response.top?.first?.malID)
    }
}
